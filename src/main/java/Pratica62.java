
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    
//    private static ArrayList<Jogador> conjunto = new ArrayList<>();
    private static ArrayList<String> cidades = new ArrayList<>();
    
    
    public static void main(String[] args) {
        // System.out.println("Olá, Java!");
        int elem_conj;
        
        
/*        conjunto.add(new Jogador(12,"Felix"));
        conjunto.add(new Jogador(10,"Roberto Rivelino"));
        conjunto.add(new Jogador(6,"Toninho"));
        conjunto.add(new Jogador(12,"Aranha"));
        conjunto.add(new Jogador(2,"Durval"));
        conjunto.add(new Jogador(2, "Carlos Alberto"));
        conjunto.add(new Jogador(6,"Pirlo"));
        conjunto.add(new Jogador(10,"Ronaldo")); */
        //7 - No seu programa principal, use o método ordena (Time)
        //criado no item anterior para ordenar um time de jogadores
        //em ordem descendente de nome e ascendente de número
        //e exiba a lista ordenada.
        // public List<Jogador> ordena(JogadorComparator valor)
        // 
        //Collections.sort(conjunto, new JogadorComparator(true,false,false));
        //for(Jogador jog : conjunto)  System.out.println(jog);
        
/*        Collections.sort(conjunto ,Collections.reverseOrder()); // Ordem reversa
        Collections.sort(conjunto);  // ordem ascendente
        for(Jogador jog : conjunto)  System.out.println(jog); */
        
        cidades.add("Rio de Janeiro");
        cidades.add("Sao Paulo");
        cidades.add("Parana");
        cidades.add("Santa Catarina");
        cidades.add("Rio Grande do Sul");
        
        //Collections.sort(cidades);
/*        for(String valor: cidades) */ //System.out.println(cidades.indexOf("Sao Paulo"));
        
/*        Collections.sort(conjunto, new Comparator<Jogador>() {
            @Override
            public int compare(Jogador joga1, Jogador joga2) {
                return joga1.getNome().compareTo(joga2.getNome());
            }
        }); */
        //Collections.sort(conjunto);
        
        //Comparator<Jogador> porNumero = (e1, e2) -> Integer.compare(
        //    e1.getNumero(), e2.getNumero());

        //conjunto.stream().sorted(porNumero)
        //    .forEach(e -> System.out.println(e));
        
        //System.out.println("\n Nova Comparacao \n");
        //for(Jogador jog : conjunto)  System.out.println(jog);
/*        Collections.sort(conjunto);
        int index = Collections.binarySearch(conjunto, new Jogador(2, null));
        if(index >= 0)
        System.out.println("O jogador de numero 2 é : "+conjunto.get(index).getNome());
        else
        System.out.println("O jogador de numero 2 nao esta na lista\n");
        
        int valor = conjunto.size();
        System.out.println("valor tamanho conjunto = "+valor);
        
        Collections.sort(conjunto);
        for (int i = conjunto.size()-2; i >= 0; --i)
        if (conjunto.get(i).equals(conjunto.get(i+1)))  
            System.out.println("Valores : "+conjunto.get(index).getNome()); */
        Time valor1 = new Time();
        // JogadorComparator(a,b,c)
        // a(1) -> Ordenação por número / a(0) -> Ordenação por nome
        // b(1) -> Ordenação número ascendente / b(0) -> Ordenação número descendente
        // c(1) -> Ordenação nome ascendente / c(0) -> Ordenação nome descendente 
        JogadorComparator valores = new JogadorComparator(false,true,false);
        List<Jogador> valor = valor1.ordena(valores);
        for(Jogador joga : valor) System.out.println(joga);
        
        int valor_joga =12;
        int achou = Collections.binarySearch(valor, new Jogador(valor_joga,null));
        if(achou>=0) System.out.println("O jogador de numero "+valor.get(achou).getNumero()+" é : "+valor.get(achou).getNome());
        else  System.out.println("O jogador de numero "+valor_joga+" nao esta na lista\n");
        
/*        JogadorComparator res0 = new JogadorComparator(true, false, false);
        List<Jogador> res3 = new ArrayList<>();
        List<Jogador> res4 = new ArrayList<>();
        res3.add(new Jogador(12,"Felix"));
        res3.add(new Jogador(10,"Roberto Rivelino"));
        res4.add(new Jogador(6,"Toninho"));
        Jogador res1 = new Jogador(12,"Felix");
        Jogador res2 = new Jogador(12,"Felix");
        //res1.setNumero(2);
        int teste = res0.compare(res1, res2);
        System.out.println("Compara Jogador = "+teste); */
    }
    
    public void teste(int valor, String nome){
        
    }
}
